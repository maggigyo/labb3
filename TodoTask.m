//
//  TodoTask.m
//  Labb3
//
//  Created by ITHS on 2016-02-12.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "TodoTask.h"


@implementation TodoTask
-(instancetype)initWithText:(NSString*)text andImportant:(BOOL)important{
    self = [super init];
    if(self){
        self.TodoText = text;
        self.done = NO;
        self.important = important;
    }
    return self;
}

@end
