//
//  TodoTableViewController.m
//  Labb3
//
//  Created by ITHS on 2016-02-12.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "TodoTableViewController.h"
#import "TodoTask.h"
#import "PropertiesView.h"

@interface TodoTableViewController ()
@property (nonatomic) NSMutableArray *tasks;
@end

@implementation TodoTableViewController

-(NSArray*) tasks{
    if(!_tasks){
        _tasks = @[[[TodoTask alloc]initWithText:@"Do dishes" andImportant:NO]
                   ,[[TodoTask alloc]initWithText:@"Take out garbage" andImportant:NO]
                   ,[[TodoTask alloc]initWithText:@"Get a haircut" andImportant:NO]
                   ,[[TodoTask alloc]initWithText:@"Apply for job" andImportant:YES]
                   ,[[TodoTask alloc]initWithText:@"See a doctor about crisp addiction" andImportant:NO]
                   ,[[TodoTask alloc]initWithText:@"Call my mum" andImportant:YES]
                   ,[[TodoTask alloc]initWithText:@"Make petition about Pokemon MMO" andImportant:YES]
                   ].mutableCopy;
    }
    return _tasks;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tasks.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:@"TodoCell" forIndexPath:indexPath];
    NSString *displayText;
    if(((TodoTask*)self.tasks[indexPath.row]).done){//If task is done
        //Puts a check mark at the beginning and the rest of the text
        displayText = [NSString stringWithFormat:@"%@  %@",@"\u2714",
                       ((TodoTask*)self.tasks[indexPath.row]).TodoText];
    }
    else{
        //Only the text
        displayText = ((TodoTask*)self.tasks[indexPath.row]).TodoText;//Adds rest of text.
    }
    cell.textLabel.text = displayText;
    
    if(((TodoTask*)self.tasks[indexPath.row]).important){//If task is important
        cell.textLabel.backgroundColor = [UIColor colorWithRed:1.0f green:0.60f blue:0.60f alpha:1.0];
    }
    return cell;
}

-(void) viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UITableViewCell *)sender {
    PropertiesView *destination = [segue destinationViewController];
    int row = (int)[self.tableView indexPathForCell:sender].row;
    TodoTask *chosenTask = self.tasks[row];
    destination.title = chosenTask.TodoText;
    destination.important = chosenTask.important;
    destination.done = chosenTask.done;
    destination.tasks = self.tasks;
    destination.index = row;
}


@end
