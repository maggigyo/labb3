//
//  PropertiesView.h
//  Labb3
//
//  Created by ITHS on 2016-02-12.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TodoTask.h"

@interface PropertiesView : UIViewController
@property  ()NSMutableArray *tasks;
@property BOOL important;
@property BOOL done;
@property TodoTask *chosenTask;
@property int index;

@end
