//
//  PropertiesView.m
//  Labb3
//
//  Created by ITHS on 2016-02-12.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "PropertiesView.h"
#import "AddTaskView.h"


@interface PropertiesView ()
@property (weak, nonatomic) IBOutlet UISwitch *importantSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *doneSwitch;

@end

@implementation PropertiesView



-(void)viewWillAppear:(BOOL)animated{
    self.importantSwitch.on = self.important;
    self.doneSwitch.on = self.done;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.importantSwitch addTarget:self
                      action:@selector(stateChanged:) forControlEvents:UIControlEventValueChanged];
    [self.doneSwitch addTarget:self
                      action:@selector(stateChanged:) forControlEvents:UIControlEventValueChanged];
    // Do any additional setup after loading the view.
}

//Can't make this thing work!
- (void)stateChanged:(UISwitch *)theSwitch {
    self.chosenTask.important = self.importantSwitch.on;
    self.chosenTask.done = self.doneSwitch.on;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    AddTaskView *destination = [segue destinationViewController];
    destination.tasks = self.tasks;
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
