//
//  TodoTask.h
//  Labb3
//
//  Created by ITHS on 2016-02-12.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TodoTask : NSObject
@property NSString *TodoText;
@property BOOL important;
@property BOOL done;
-(instancetype)initWithText:(NSString*)text andImportant:(BOOL)important;

@end
