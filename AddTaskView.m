//
//  AddTaskView.m
//  Labb3
//
//  Created by ITHS on 2016-02-12.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "AddTaskView.h"
#import "TodoTask.h"

@interface AddTaskView ()
@property (weak, nonatomic) IBOutlet UITextField *descriptionText;
@property (weak, nonatomic) IBOutlet UISwitch *importantSwitch;

@end

@implementation AddTaskView

- (IBAction)addTask:(UIButton *)sender {
    [self.tasks addObject:[
                           [TodoTask alloc]initWithText:self.descriptionText.text
                           andImportant:self.importantSwitch.isOn]
     ];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
